(() => {
    'use strict'
  
    function setActiveItem() {
        const path = location.pathname

        const link = document.querySelector(`.docs-nav a[href="${path}"]`)
  
        if (!link) {
            return
        }
  
        const parent = link.parentNode.parentNode.parentNode.previousElementSibling

        link.classList.add('active')

        if (parent.classList.contains('collapsed')) {
            parent.click()
        }
    }
  
    window.addEventListener('load', setActiveItem)
})()
