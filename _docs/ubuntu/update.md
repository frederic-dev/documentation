---
title: "Update!"
---
It's always good practice to ensure your system is up to date.

You can update Ubuntu using the terminal.

Press **Ctrl+Alt+T** to bring up a Terminal window (or click the terminal icon in the sidebar).

Check for updates:

```bash
sudo apt update
```

You will be prompted to enter your login password.

To apply any updates:

```bash
sudo apt upgrade
```

Type **Y**, then press **Enter** to confirm to finish the update process.