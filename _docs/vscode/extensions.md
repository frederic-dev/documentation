---
title: "Extensions"
---
## Browse for extensions
Press **Ctrl+Shift+X** to bring up the Extensions view (or click on the Extensions icon in the Activity Bar on the side).

## Dracula Official

*Identifier: dracula-theme.theme-dracula*

Official Dracula Theme. A dark theme for many editors, shells, and more.

## Material Icon Theme

*Identifier: pkief.material-icon-theme*

Material Design Icons for Visual Studio Code

## PHP Intelephense

*Identifier: bmewburn.vscode-intelephense-client*

PHP code intelligence for Visual Studio Code

## Twig Language 2

*Identifier: mblode.twig-language-2*

Snippets, Syntax Highlighting, Hover, and Formatting for Twig

### Associate HTML files as twig syntax

Configure file associations to languages:

- From **File** > **Preferences** > **Settings** (`Ctrl+,`), search for `Files: Associations`.

- Click the **Add Item** button.

- Set **Key** to `*.html` and **Value** to `twig`.

If you use the JSON editor for your settings, add the following line:

```json
"files.associations": {
    "*.html": "twig"
}
```

### Get emmet working

Add a mapping here between the language and Emmet supported language:

- From **File** > **Preferences** > **Settings** (`Ctrl+,`), search for `Emmet: Include Languages`.

- Click the **Add Item** button.

- Set **Key** to `twig` and **Value** to `html`.

If you use the JSON editor for your settings, add the following line:

```json
"emmet.includeLanguages": {
    "twig": "html"
}
```

## phpcs

*Identifier: shevaua.phpcs*

PHP CodeSniffer for Visual Studio Code

### Installation

Before using this plugin, you must ensure that *phpcs* is installed on your system. The preferred method is using composer.

The phpcs linter can be installed globally using the Composer Dependency Manager for PHP.

```bash
composer global require "squizlabs/php_codesniffer=*"
```

### Basic Configuration

**phpcs.executablePath**

This setting controls the executable path for the phpcs. You may specify the absolute path or workspace relative path to the phpcs executable.

From **File** > **Preferences** > **Settings** (`Ctrl+,`), search for `Phpcs: Executable Path`.


Set the path to the phpcs executable:
```
/home/user/.config/composer/vendor/bin/phpcs 
```

**phpcs.standard**

This setting controls the coding standard used by phpcs.

From **File** > **Preferences** > **Settings** (`Ctrl+,`), search for `Phpcs: Standard`.

Edit in settings.json
```json
"phpcs.standard": "PSR12"
```
