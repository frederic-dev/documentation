---
title: "Install on Ubuntu"
---
## Installation

The easiest way to install Visual Studio Code for Ubuntu is to download the ***.deb package (64-bit)*** on the [Download Visual Studio Code](https://code.visualstudio.com/Download) page.

Install the package through the command line with:

```bash
sudo apt install ./<file>.deb
# sudo apt install ./code_1.70.0-1659589288_amd64.deb
```

## Telemetry

### Disable telemetry reporting

From **File** > **Preferences** > **Settings** (`Ctrl+,`), search for `telemetry`.

Set the **Telemetry: Telemetry Level** setting to `off`.

If you use the JSON editor for your settings (`Ctrl+Shift+P`, search for `Preferences: Open User Settings (JSON)`), add the following line:

```json
"telemetry.telemetryLevel": "off"
```
